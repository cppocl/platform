/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_INTERNAL_PLATFORMS_HPP
#define OCL_GUARD_PLATFORM_INTERNAL_PLATFORMS_HPP

// List all supported operating systems
#define OCL_PLATFORM_UNKNOWN 0
#define OCL_PLATFORM_ANDROID 1
#define OCL_PLATFORM_BSD     2
#define OCL_PLATFORM_LINUX   3
#define OCL_PLATFORM_MAC     4
#define OCL_PLATFORM_UNIX    5
#define OCL_PLATFORM_WINDOWS 6

#endif // OCL_GUARD_PLATFORM_INTERNAL_PLATFORMS_HPP
