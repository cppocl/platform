/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORMS_HPP
#define OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORMS_HPP

#ifndef OCL_PLATFORM_LINUX
#error platforms.hpp must be uncluded!
#endif

#define OCL_OS_LINUX (OCL_PLATFORM_LINUX * 100)

#endif // OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORMS_HPP
