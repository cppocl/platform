/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORM_DETECTION_HPP
#define OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORM_DETECTION_HPP

#ifndef OCL_PLATFORM_LINUX
#error platforms.hpp must be included!
#endif

#ifdef OCL_PLATFORM
#error Platform already detected!
#endif

#if defined(__GNU__)
#define OCL_PLATFORM OCL_PLATFORM_LINUX
#define OCL_PLATFORM_DEFINE __GNU__
#define OCL_PLATFORM_POSIX true
#endif

#if !defined(OCL_PLATFORM) && defined(__GNUC__)
#define OCL_PLATFORM OCL_PLATFORM_LINUX
#define OCL_PLATFORM_DEFINE __GNUC__
#define OCL_PLATFORM_POSIX true
#endif

#if !defined(OCL_PLATFORM) && defined(__gnu_linux__)
#define OCL_PLATFORM OCL_PLATFORM_LINUX
#define OCL_PLATFORM_DEFINE __gnu_linux__
#define OCL_PLATFORM_POSIX true
#endif

#if !defined(OCL_PLATFORM) && defined(__linux__)
#define OCL_PLATFORM OCL_PLATFORM_LINUX
#define OCL_PLATFORM_DEFINE __linux__
#define OCL_PLATFORM_POSIX true
#endif

#endif // OCL_GUARD_PLATFORM_INTERNAL_LINUX_LINUX_PLATFORM_DETECTION_HPP
