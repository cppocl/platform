/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_INTERNAL_WIN_WIN_PLATFORMS_HPP
#define OCL_GUARD_PLATFORM_INTERNAL_WIN_WIN_PLATFORMS_HPP

#ifndef OCL_PLATFORM_WINDOWS
#error platforms.hpp must be uncluded!
#endif

#define OCL_OPERATING_SYSTEM_WINDOWS_30        (OCL_PLATFORM_WINDOWS * 100)
#define OCL_OPERATING_SYSTEM_WINDOWS_31        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 1)
#define OCL_OPERATING_SYSTEM_WINDOWS_95        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 2)
#define OCL_OPERATING_SYSTEM_WINDOWS_98        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 3)
#define OCL_OPERATING_SYSTEM_WINDOWS_MILLENIUM (OCL_OPERATING_SYSTEM_WINDOWS_30 + 4)
#define OCL_OPERATING_SYSTEM_WINDOWS_NT        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 5)
#define OCL_OPERATING_SYSTEM_WINDOWS_2000      (OCL_OPERATING_SYSTEM_WINDOWS_30 + 6)
#define OCL_OPERATING_SYSTEM_WINDOWS_XP        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 7)
#define OCL_OPERATING_SYSTEM_WINDOWS_VISTA     (OCL_OPERATING_SYSTEM_WINDOWS_30 + 8)
#define OCL_OPERATING_SYSTEM_WINDOWS_7         (OCL_OPERATING_SYSTEM_WINDOWS_30 + 9)
#define OCL_OPERATING_SYSTEM_WINDOWS_80        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 10)
#define OCL_OPERATING_SYSTEM_WINDOWS_81        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 11)
#define OCL_OPERATING_SYSTEM_WINDOWS_10        (OCL_OPERATING_SYSTEM_WINDOWS_30 + 12)

#endif // OCL_GUARD_PLATFORM_INTERNAL_WIN_WIN_PLATFORMS_HPP
