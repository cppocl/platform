/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_INTERNAL_PLATFORM_DETECTION_HPP
#define OCL_GUARD_PLATFORM_INTERNAL_PLATFORM_DETECTION_HPP

#ifdef OCL_PLATFORM
#error Platform is already detected!
#endif

#ifndef OCL_PLATFORM_WINDOWS
#error platforms.hpp must be included before platform_detection.hpp
#endif

// Detect the compiler and compiler defines to detect the platform.
// After detection defines OCL_PLATFORM, OCL_PLATFORM_DEFINE and OCL_PLATFORM_POSIX
// will be defined.

#include "win/win_platform_detection.hpp"

#if !defined(OCL_PLATFORM)
#include "linux/linux_platform_detection.hpp"
#endif

#if !defined(OCL_PLATFORM)
#include "mac/mac_platform_detection.hpp"
#endif

#if !defined(OCL_PLATFORM)
#include "android/android_platform_detection.hpp"
#endif

// If no platform can be identified, then mark the platform as unknown,
// giving existing C++ a change to compile portable code.
#if !defined(OCL_PLATFORM)
#include "unknown/unknown_platform_detection.hpp"
#endif

// NOTE: Either a platform or unknown platform should always be set.
#if !defined(OCL_PLATFORM)
#error Platform detection mechanism has failed!
#endif

#endif // OCL_GUARD_PLATFORM_INTERNAL_PLATFORM_DETECTION_HPP
