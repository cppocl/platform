/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_PLATFORM_PLATFORM_HPP
#define OCL_GUARD_PLATFORM_PLATFORM_HPP

/*
    HOWTO:
        Adding new platforms requires adding the new platform to internal/platforms.hpp
        and adding a new folder which will contain the new <platform>_platform_detection.hpp header file.
*/

#include "internal/platforms.hpp"
#include "internal/platform_detection.hpp"

namespace ocl
{

enum class PlatformTypes
{
    Unknown = OCL_PLATFORM_UNKNOWN,
    Windows = OCL_PLATFORM_WINDOWS,
    Linux   = OCL_PLATFORM_LINUX,
    Unix    = OCL_PLATFORM_UNIX,
    BSD     = OCL_PLATFORM_BSD,
    Mac     = OCL_PLATFORM_MAC,
    Android = OCL_PLATFORM_ANDROID
};

#if !defined(OCL_PLATFORM)
#error Platform must always be defined, even if its defined as unknown!
#endif

constexpr PlatformTypes platform_type =
#if OCL_PLATFORM == OCL_PLATFORM_WINDOWS
    PlatformTypes::Windows
#elif OCL_PLATFORM == OCL_PLATFORM_LINUX
    PlatformTypes::Linux
#elif OCL_PLATFORM == OCL_PLATFORM_UNIX
    PlatformTypes::Unix
#elif OCL_PLATFORM == OCL_PLATFORM_BSD
    PlatformTypes::BSD
#elif OCL_PLATFORM == OCL_PLATFORM_MAC
    PlatformTypes::Mac
#elif OCL_PLATFORM == OCL_PLATFORM_ANDROID
    PlatformTypes::Android
#endif
    ;

constexpr char const platform_type_string[] =
#if OCL_PLATFORM == OCL_PLATFORM_WINDOWS
    "Windows"
#elif OCL_PLATFORM == OCL_PLATFORM_LINUX
    "Linux"
#elif OCL_PLATFORM == OCL_PLATFORM_UNIX
    "Unix"
#elif OCL_PLATFORM == OCL_PLATFORM_BSD
    "BSD"
#elif OCL_PLATFORM == OCL_PLATFORM_MAC
    "Mac"
#elif OCL_PLATFORM == OCL_PLATFORM_ANDROID
    "Android"
#endif
    ;

} // namespace ocl

#endif // OCL_GUARD_PLATFORM_PLATFORM_HPP
